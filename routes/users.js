var express = require('express');
var router = express.Router();

/* GET users listing. */
module.exports = (pool) => {
  router.get('/', async (req, res, next) => {
    const query = `
      SELECT id, username, created_at, updated_at FROM users ORDER BY id;
    `;
    try {
      const { rows } = await pool.query(query);
      res.status(200).json({
        status: 200,
        success: true,
        content: rows
      })
    } catch (error) {
      console.error(error);
      res.status(500).json({
        status: 500,
        success: false,
        message: error.message
      });
    }
  });

  router.get('/:id', async (req, res, next) => {
    const { id } = req.params;
    const query = `
      SELECT id, username, created_at, updated_at FROM users WHERE id=${id};
    `;
    try {
      const { rows } = await pool.query(query);
      res.status(200).json({
        status: 200,
        success: true,
        content: rows[0]
      })
    } catch (error) {
      console.error(error);
      res.status(500).json({
        status: 500,
        success: false,
        message: error.message
      });
    }
  });

  router.post('/', async (req, res, next) => {
    const { username, password } = req.body;
    const querySelect = `SELECT id, username from users WHERE username='${username}'`;
    const queryInsert = `
      INSERT INTO users(username, password) VALUES ($1, $2) RETURNING id, username, created_at, updated_at
    `;
    try {
      const { rows: user } = await pool.query(querySelect);
      if (user.length > 0) {
        return res.status(400).json({
          status: 400,
          success: false,
          message: 'Username already exists.'
        });
      }

      const { rows: inserted } = await pool.query(queryInsert, [username, password]);
      res.status(201).json({
        status: 201,
        success: true,
        content: inserted[0],
      })
    } catch (error) {
      console.error(error);
      res.status(500).json({
        status: 500,
        success: false,
        message: error.message
      });
    }
  })

  router.put('/:id', async (req, res, next) => {
    const { id } = req.params;
    const { username, password } = req.body;
    const querySelect = `SELECT id from users where = ${id}`;
    const queryUpdate = `
      UPDATE users SET username=$1, password=$2, updated_at=NOW() 
      WHERE = $3 
      RETURNING *
    `;

    try {
      const { rows: selected } = await pool.query(querySelect);
      if (selected.length === 0) {
        return res.status(404).json({
          status: 404,
          success: false,
          message: 'resource not found.'
        });
      }

      const { rows: updated } = await pool.query(queryUpdate, [username, password, id]);
      res.status(200).json({
        status: 200,
        success: true,
        content: updated[0],
      });
    } catch (error) {
      console.error(error);
      res.status(500).json({
        status: 500,
        success: false,
        message: error.message,
      });
    }
  });

  router.delete('/:id', async (req, res, next) => {
    const { id } = req.params;
    const querySelect = `SELECT id FROM users where id=${id}`;
    const queryDelete = `DELETE FROM users where id=${id}`;

    try {
      const { rows: selected } = await pool.query(querySelect);
      if (selected.length === 0) {
        return res.status(404).json({
          status: 404,
          success: false,
          message: 'resource not found.'
        });
      }

      await pool.query(queryDelete);
      res.status(200).json({
        status: 200,
        success: true,
        message: 'resource deleted.'
      })
    } catch (error) {
      console.error(error);
      res.status(500).json({
        status: 500,
        success: false,
        message: error.message,
      });
    }
  });

  router.post('/login', async (req, res) => {
    const { username, password } = req.body;
    const query = `SELECT username, password from users where username=$1`;
    try {
      const { rows } = await pool.query(query, [username]);
      if (rows.length === 0) {
        return res.status(400).json({
          status: 400,
          success: false,
          message: 'username or password does not match.'
        });
      }

      if (rows[0].password !== password) {
        return res.status(400).json({
          status: 400,
          success: false,
          message: 'username or password does not match.'
        });
      }

      res.status(200).json({
        status: 200,
        success: true,
        message: 'login successfully',
        content: rows[0]
      })
    } catch (error) {
      console.error(error);
      res.status(500).json({
        status: 500,
        success: false,
        message: error.message,
      });
    }
  })

  return router;
};
