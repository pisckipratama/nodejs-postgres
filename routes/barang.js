const express = require('express');
const router = express.Router();

module.exports = (pool) => {
  /* GET home page. */
  router.get('/', async (req, res) => {
    const query = 'SELECT * FROM barang ORDER BY id;';

    try {
      const { rows } = await pool.query(query);
      res.status(200).json({
        status: 200,
        success: true,
        content: rows
      });
    } catch (error) {
      console.error(error);
      res.status(500).json({
        status: 500,
        success: false,
        message: error.message
      });
    }
  });

  router.get('/:id', async (req, res) => {
    const { id } = req.params;
    console.log(id);
    const query = `SELECT * FROM barang WHERE id=$1`;

    try {
      const { rows } = await pool.query(query, [parseInt(id)]);
      console.log(rows);
      if (rows.length <= 0) {
        res.status(404).json({
          status: 404,
          success: false,
          message: 'data not found'
        });
      }

      res.json({
        status: 200,
        success: true,
        content: rows[0]
      });
    } catch (error) {
      console.error(error);
      res.status(500).json({
        status: 500,
        success: false,
        message: error.message
      })
    }
  })

  router.post('/', async (req, res) => {
    const {
      nama,
      merk,
      keterangan
    } = req.body;

    try {
      const query = `
        INSERT INTO barang(nama, merk, keterangan)
        VALUES($1, $2, $3)
        RETURNING *
      `;
      const values = [nama, merk, keterangan];
      const { rows } = await pool.query(query, values);
      res.status(201).json({
        status: 201,
        success: true,
        message: 'data created',
        content: rows[0]
      });
    } catch (error) {
      console.error(error);
      res.status(500).json({
        status: 500,
        success: false,
        message: error.message
      })
    }
  });

  router.delete('/:id', async (req, res) => {
    const { id } = req.params;
    const query = `DELETE FROM barang WHERE id=${id}`;

    try {
      const querySelect = `SELECT id FROM barang WHERE id=${id}`;
      const { rows } = await pool.query(querySelect);
      if (rows.length <= 0) {
        return res.status(404).json({
          status: 404,
          success: false,
          message: 'data not found'
        });
      }

      await pool.query(query);
      return res.json({
        status: 204,
        success: true,
        message: 'data deleted'
      });
    } catch (error) {
      console.error(error);
      res.status(500).json({
        status: 500,
        success: false,
        message: error.message
      })
    }
  });

  router.put('/:id', async (req, res) => {
    const { id } = req.params;
    const {
      nama,
      merk,
      keterangan
    } = req.body;

    const query = `
      UPDATE barang SET nama=$1, merk=$2, keterangan=$3, updated_at=NOW() 
      WHERE id=$4
      RETURNING *
    `;

    const values = [nama, merk, keterangan, id];

    try {
      const querySelect = `SELECT id FROM barang WHERE id=${id}`;
      const { rows: result } = await pool.query(querySelect);
      if (result.length <= 0) {
        res.status(404).json({
          status: 404,
          success: false,
          message: 'data not found'
        });
      }

      const { rows } = await pool.query(query, values);
      res.status(200).json({
        status: 200,
        success: true,
        message: 'data updated',
        content: rows[0],
      });
    } catch (error) {
      console.error(error);
      res.status(500).json({
        status: 500,
        success: false,
        message: error.message
      });
    }
  });

  return router;
};
