CREATE TABLE users
(
  id serial PRIMARY KEY NOT NULL,
  username character varying(50) NOT NULL,
  password character varying(100) NOT NULL,
  created_at timestamp DEFAULT NOW(),
  updated_at timestamp DEFAULT NOW()
);

CREATE TABLE barang
(
  id serial PRIMARY KEY NOT NULL,
  nama character varying(70) NOT NULL,
  merk character varying(50) NOT NULL,
  keterangan text,
  created_at timestamp DEFAULT NOW(),
  updated_at timestamp DEFAULT NOW()
);