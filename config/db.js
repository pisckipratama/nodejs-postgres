const { Pool } = require('pg');

const pool = new Pool({
  user: "postgres",
  host: "192.168.2.151",
  database: "cikaso",
  password: "P@ssw0rd123!",
  port: 5432
});

module.exports = { pool };