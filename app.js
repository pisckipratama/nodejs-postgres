const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');

const { pool } = require('./config/db');

const indexRouter = require('./routes/barang')(pool);
const usersRouter = require('./routes/users')(pool);

const app = express();

app.use(logger('dev'));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/barang', indexRouter);
app.use('/users', usersRouter);

module.exports = app;
