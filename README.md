## CRUD NodeJS Postgres

for running this app please run following this command
- npm install
- npm start
- open http://localhost:3000

#### Users Method
```GET /users``` <br>
```GET /users/:id``` <br>
```POST /users```
```json
header:
{
  "Content-Type": "application/json"
}

body:
{
  "username": "john",
  "password": "rctioke123"
}
```
<br>

`PUT /users/:id`
```json
header:
{
  "Content-Type": "application/json"
}

body:
{
  "username": "john",
  "password": "rctioke123"
}
```


`DELETE /users/:id`

#### Barang Method
`GET /barang` <br>
`GET /barang/:id` <br>
`POST /barang`
```json
header:
{
  "Content-Type": "application/json"
}

body:
{
  "nama": "Laptop",
  "merk": "Lenovo",
  "keterangan": "Ram 8GB, Ryzen 4200, SSD 512GB."
}
```
<br>

`PUT /barang/:id`
```json
header:
{
  "Content-Type": "application/json"
}

body:
{
  "nama": "Laptop",
  "merk": "Lenovo",
  "keterangan": "Ram 8GB, Ryzen 4200, SSD 512GB."
}
```
<br>

`DELETE /barang/:id`